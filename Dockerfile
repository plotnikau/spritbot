FROM    scaleway/java

COPY    spritbot/target/* ./spritbot.jar

CMD     java -jar spritbot.jar

