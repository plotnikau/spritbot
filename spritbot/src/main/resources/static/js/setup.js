function setup() {
    var newAddress = {
        street: $("#new-street").val(),
        housenumber: $("#new-housenumber").val(),
        city: $("#new-city").val(),
        state: getParameterByName('state'),
        client_id: getParameterByName('client_id'),
        scope: getParameterByName('scope'),
        response_type: getParameterByName('response_type'),
        redirect_uri: getParameterByName('redirect_uri')
    };

    var newData=JSON.stringify(newAddress);

    $.ajax({
        method: "PUT",
        url: "/tankstelle/setup",
        dataType: "json",
        data: newData,
        contentType: 'application/json; charset=utf-8'
    })
    .done(function () {
        //TODO go back
        alert("done");
    })
    .fail(function (e,a,b) {
        alert("should not happen"+e+a+b);
    });
    return false;
}

function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}