package com.rp.bot.service;

import com.rp.bot.model.Brand;
import com.rp.bot.model.FuelType;
import com.rp.bot.model.response.fuel.GasStation;

public class FormattingHelper {
    public static String prepareResponse(GasStation[] gasStations, FuelType preferredFuelType, Brand preferredBrand) {
        if (gasStations == null || gasStations.length == 0) {
            return "No gas stations near you";
        }

        String responseText;
        StringBuilder sb = new StringBuilder();
        for (GasStation gasStation : gasStations) {
            if ((!Brand.ALL.equals(preferredBrand) && !gasStation.getBrand().equalsIgnoreCase(preferredBrand.toString()))) {
                continue;
            }

            if (gasStation.getE10() == 0 && gasStation.getE5() == 0 && gasStation.getDiesel() == 0 && gasStation.getPrice() == null) {
                return "Marelli code is disabled, no stations found for you";
            }
            sb.append(gasStation.getMdFormattedDescription());
            if (gasStation.getPrice() != null) {
                sb.append(" - ").append(gasStation.getPrice());
            }
            if (gasStation.getDiesel() != null) {
                if (preferredFuelType == null || FuelType.ALL.equals(preferredFuelType) || FuelType.DIESEL.equals(preferredFuelType)) {
                    sb.append("\ndiesel - ").append(gasStation.getDiesel());
                }
            }
            if (gasStation.getE10() != null) {
                if (preferredFuelType == null || FuelType.ALL.equals(preferredFuelType) || FuelType.E10.equals(preferredFuelType)) {
                    sb.append("\ne10 - ").append(gasStation.getE10());
                }
            }
            if (gasStation.getE5() != null) {
                if (preferredFuelType == null || FuelType.ALL.equals(preferredFuelType) || FuelType.E5.equals(preferredFuelType)) {
                    sb.append("\nsuper - ").append(gasStation.getE5());
                }
            }
            sb.append("\n\n");
        }

        responseText = sb.toString();
        if (responseText.equalsIgnoreCase("")) {
            responseText = "No " + preferredBrand.toString() + " gas stations near you";
        }

        return responseText;
    }

}
