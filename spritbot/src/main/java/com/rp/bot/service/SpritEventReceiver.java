//package com.rp.bot.service;
//
//import com.rp.bot.SpritBotProperties;
//import com.rp.bot.model.response.fuel.GasStation;
//import com.rp.bot.model.response.fuel.StationFuelPrice;
//import com.rp.bot.repositories.UsersRepository;
//import de.bots.TelegramAPI;
//import de.bots.model.KeyboardButton;
//import de.bots.model.Location;
//import de.bots.model.ReplyKeyboardMarkup;
//import de.bots.model.Update;
//import de.bots.model.response.UpdateResponse;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//
//import java.util.*;
//import java.util.concurrent.atomic.AtomicLong;
//
////@Component
//public class SpritEventReceiver {
//
//    @Autowired
//    private TelegramAPI methods;
//
//    @Autowired
//    private FuelService fuelService;
//
//    @Autowired
//    private UsersRepository usersRepository;
//
//    private String botName;
//
//    private List<String> accessTokens = new ArrayList<>();
//
//    private Set<Integer> chatIds = new HashSet<>();
//
//    private HashMap<Integer, UserPresets> clientsPresets = new HashMap<>();
//    private HashMap<Integer, String> trackedStationIds = new HashMap<>();
//
//    private AtomicLong eventID = new AtomicLong(0L);
//
//    private static final Logger LOG = Logger.getLogger(SpritEventReceiver.class);
//    private String helloMessage = "I serve you with pleasure.";
//
//    private String pendingCommand;
//
//    private GasStation[] gasStations;
//
//    @Autowired
//    public SpritEventReceiver(SpritBotProperties botProperties) {
//        this.botName = botProperties.getBotName();
//        newToken();
//    }
//
//    private String newToken() {
//        UUID uuid = UUID.randomUUID();
//        String tokenAsString = uuid.toString();
//        accessTokens.add(tokenAsString);
//        LOG.info("new token generated: " + uuid);
//        LOG.info("use this link to add a bot to chat: https://telegram.me/" + botName + "?start=" + uuid);
//        return tokenAsString;
//    }
//
//    @Scheduled(fixedRate = 1000L)
//    public void handleEvents() {
//        Long id = new Long(eventID.toString());
//        UpdateResponse updates = methods.getUpdates(id);
//
//        if (updates.isOk()) {
//            Update[] result = updates.getResult();
//            for (Update update : result) {
//                LOG.info("Received update: " + update);
//                processEvent(update);
//                eventID.set(update.getUpdate_id() + 1);
//            }
//        }
//    }
//
//    //@Scheduled(fixedRate = 2*1000L)
//    public void trackFuelPrices() {
//        LOG.info("tracking prices");
//        Iterator iterator = trackedStationIds.keySet().iterator();
//        while (iterator.hasNext()) {
//            Integer chatId = (Integer) iterator.next();
//            String stationId = trackedStationIds.get(chatId);
//            checkPricesForStation(chatId, stationId);
//        }
//
//    }
//
//    private void checkPricesForStation(Integer chatId, String stationId) {
//        StationFuelPrice priceInfo = fuelService.getPricesForStation(stationId);
//        if (priceInfo != null) {
//            updatePricesForStation(chatId, stationId, priceInfo);
//        }
//    }
//
//    private void processEvent(Update update) {
//        handleTextMessage(update);
//    }
//
//    private void handleTextMessage(Update update) {
//
//        int chatId = update.getMessage().getChat().getId();
//        String text = update.getMessage().getText();
//        Location location = update.getMessage().getLocation();
//
//        if (text == null && location == null) {
//            LOG.warn("none of text or location received");
//            text = "I am text/location-only bot. Give me some text or location...";
//            methods.sendMessage(chatId, "did not understand your input: " + text);
//        }
//
//        if (text != null) {
//            if (text.startsWith("?")) {
//                sendActionsKeyboard(chatId);
//                return;
//            }
//            handleBotCommand(text, chatId);
//        }
//        if (location != null) {
//            handleLocation(location, chatId);
//        }
//    }
//
//    private void handleLocation(Location location, int chatId) {
//        if (pendingCommand == null || !pendingCommand.equalsIgnoreCase("track")) {
//            //updateClientLastLocation(chatId, location);
//        }
//
//        //getStationsForLocation(chatId, location.getLatitude(), location.getLongitude());
//    }
//
//
//
//    private void updateClientFuelType(int chatId, String fuelType) {
//        UserPresets userPresets = clientsPresets.get(chatId);
//        if (userPresets == null) {
//            userPresets = new UserPresets();
//        }
//        //TODO: refactor: userPresets.setFuelType(fuelType);
//        clientsPresets.put(chatId, userPresets);
//    }
//
//    private void updateStationForPriceTracking(int chatId, GasStation station) {
//        trackedStationIds.put(chatId, station.getId());
//
//        UserPresets userPresets = clientsPresets.get(chatId);
//        if (userPresets == null) {
//            userPresets = new UserPresets();
//        }
//        userPresets.setStationForPriceTracking(station);
//        clientsPresets.put(chatId, userPresets);
//    }
//
//    private void updatePricesForStation(Integer chatId, String stationId, StationFuelPrice priceInfo) {
//        UserPresets userPresets = clientsPresets.get(chatId);
//        if (userPresets == null) {
//            userPresets = new UserPresets();
//        }
//        userPresets.setPricesForTrackedStation(priceInfo);
//        clientsPresets.put(chatId, userPresets);
//    }
//
//    private void handleBotCommand(String text, int chatId) {
//        String[] tokens = text.split(" ");
//        String command = tokens[0].toLowerCase();
//
//        String responseText = "I didn't understand your input: " + text;
//
//        chatIds.add(chatId);
//
//        switch (command) {
//            case "/start":
//                sendMessageWithLocationsKeyboard(chatId, helloMessage);
//                return;
//            case "/newtoken":
//                String tokenString = newToken();
//                responseText = "Use this link to add a bot to chat: https://telegram.me/"+botName+"?start=" + tokenString;
//                sendMessageWithLocationsKeyboard(chatId, responseText);
//                return;
//            case "/ids":
//                StringBuilder builder = new StringBuilder();
//                builder.append("Chat IDs:\n");
//                for (Integer chat : chatIds) {
//                    builder.append(chat).append("\n");
//                }
//                responseText = builder.toString();
//                sendMessageWithLocationsKeyboard(chatId, responseText);
//                return;
//            case "/idadd":
//                responseText = "Failed!";
//
//                if (tokens.length >= 2) {
//                    try {
//                        Integer chatIdFromToken = Integer.valueOf(tokens[1]);
//                        chatIds.add(chatIdFromToken);
//                        responseText = "Done!";
//                    } catch (NumberFormatException ex) {
//                        LOG.error("not a valid chatId: " + tokens[1]);
//                    }
//                }
//                sendMessageWithLocationsKeyboard(chatId, responseText);
//                return;
//            case "/sethome":
//                Location lastLocation = usersRepository.getLastLocation(chatId);
//                if (lastLocation == null) {
//                    sendMessageWithLocationsKeyboard(chatId, "share a location with me");
//                    return;
//                }
//
//                usersRepository.setHomeLocation(chatId, lastLocation);
//                sendMessageWithLocationsKeyboard(chatId, "Location " + lastLocation.getLatitude() + "; " + lastLocation.getLongitude() + " set as home");
//
//                return;
//            case "track":
//                pendingCommand = command;
//                sendMessageWithLocationsKeyboard(chatId, "Select where to track the prices from list or send a location");
//                return;
////            case "home":
////                Location home = usersRepository.getHomeLocation(chatId);
////                if (home == null) {
////                    sendMessageWithLocationsKeyboard(chatId, "First share a location with me and call /sethome");
////                    return;
////                }
////                //getStationsForLocation(chatId, home.getLatitude(), home.getLongitude());
////                return;
//            case "nearme":
//                sendRequestLocationKeyboard(chatId);
//                return;
//            case "settings":
//                sendMessageWithSettingsKeyboard(chatId, "What shall be do now?");
//                return;
//            case "back":
//                sendMessageWithLocationsKeyboard(chatId, "Ok, going back");
//                return;
//            case "set fuel type":
//                applySettings(chatId, tokens);
//                return;
//            case "diesel":
//                updateClientFuelType(chatId, command);
//                sendMessageWithSettingsKeyboard(chatId, command + " set as preferred fuel type");
//                return;
//            case "e5":
//                updateClientFuelType(chatId, command);
//                sendMessageWithSettingsKeyboard(chatId, command + " set as preferred fuel type");
//                return;
//            case "e10":
//                updateClientFuelType(chatId, command);
//                sendMessageWithSettingsKeyboard(chatId, command + " set as preferred fuel type");
//                return;
//            case "all":
//                updateClientFuelType(chatId, null);
//                sendMessageWithSettingsKeyboard(chatId, command + " set as preferred fuel type");
//                return;
//            default:
//                break;
//        }
//
//        if (pendingCommand != null && pendingCommand.equalsIgnoreCase("track")) {
//            boolean trackingSet = setPriceTrackingForStation(chatId, text);
//            if (trackingSet) {
//                sendMessageWithLocationsKeyboard(chatId, "Fuel price tracking set for station: " + text);
//            }
//            else {
//                sendMessageWithLocationsKeyboard(chatId, "Unable to set price tracking for station: " + text);
//            }
//            pendingCommand = null;
//        }
//
//        sendMessageWithLocationsKeyboard(chatId, responseText);
//
//    }
//
//    private void applySettings(int chatId, String[] tokens) {
//        if (tokens.length == 1) {
//            sendMessageWithSettingsKeyboard(chatId, "Invalid input");
//            return;
//        }
//
//        switch (tokens[1]) {
//            case "fuel":
//                sendMessageWithFuelTypeKeyboard(chatId);
//                return;
//            default:
//                break;
//        }
//
//        sendMessageWithSettingsKeyboard(chatId, "Invalid input");
//
//
//    }
//
//    private boolean setPriceTrackingForStation(int chatId, String text) {
//        if (gasStations == null || text == null) {
//            return false;
//        }
//
//        for (GasStation gasStation : gasStations) {
//            if (text.equalsIgnoreCase(gasStation.getBrand() + " - " + gasStation.getName())) {
//                updateStationForPriceTracking(chatId, gasStation);
//                return true;
//            }
//        }
//
//        return false;
//
//    }
//
//
//    private void sendActionsKeyboard(int chatId) {
//        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
//        replyMarkup.setOne_time_keyboard(false);
//        replyMarkup.setResize_keyboard(true);
//
//        KeyboardButton requestLocationButton = new KeyboardButton("Near me");
//        requestLocationButton.setRequest_location(true);
//
//        KeyboardButton[][] markup = {
//                                {new KeyboardButton("Home")},
//                                {requestLocationButton}
//                            };
//
//        replyMarkup.setKeyboard(markup);
//
//        methods.sendMessage(chatId, "Fuel price: ", false, 0, replyMarkup);
//    }
//
//    private void sendLocationRequestKeyboard(int chatId) {
//        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
//        replyMarkup.setOne_time_keyboard(true);
//        replyMarkup.setResize_keyboard(true);
//
//        KeyboardButton requestLocationButton = new KeyboardButton("Near me");
//        requestLocationButton.setRequest_location(true);
//
//        KeyboardButton[][] markup = {
//                                {requestLocationButton}
//                            };
//
//        replyMarkup.setKeyboard(markup);
//
//        methods.sendMessage(chatId, "Tell me a location", false, 0, replyMarkup);
//    }
//
//    private void sendMessageWithSettingsKeyboard(int chatId, String message) {
//        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
//        replyMarkup.setOne_time_keyboard(false);
//        replyMarkup.setResize_keyboard(true);
//
//        KeyboardButton[][] markup = {
//                                {new KeyboardButton("Set fuel type")},
//                                {new KeyboardButton("Back")}
//                            };
//
//        replyMarkup.setKeyboard(markup);
//
//        methods.sendMessage(chatId, message, false, 0, replyMarkup);
//    }
//
//    private void sendRequestLocationKeyboard(int chatId) {
//        sendMessageWithLocationsKeyboard(chatId, "Share location");
//    }
//
//    private void sendMessageWithLocationsKeyboard(int chatId, String text) {
//        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
//        replyMarkup.setOne_time_keyboard(false);
//        replyMarkup.setResize_keyboard(true);
//
//        KeyboardButton requestLocationButton = new KeyboardButton("Near me");
//        requestLocationButton.setRequest_location(true);
//
//        KeyboardButton[][] markup = {
//                {new KeyboardButton("Home")},
//                {requestLocationButton},
//                {new KeyboardButton("Settings")}
//        };
//
//        replyMarkup.setKeyboard(markup);
//
//        methods.sendMessage(chatId, text, TelegramAPI.TextMessageParseMode.MARKDOWN, false, 0, replyMarkup);
//    }
//
//    private void sendMessageWithStationsKeyboard(int chatId, String text, GasStation[] gasStations) {
//        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
//        replyMarkup.setOne_time_keyboard(true);
//        replyMarkup.setResize_keyboard(true);
//
//        KeyboardButton[][] markup = new KeyboardButton[gasStations.length][1];
//        for (int i=0; i<gasStations.length; i++) {
//            markup[i][0] = new KeyboardButton(gasStations[i].getBrand() + " - " + gasStations[i].getName());
//        }
//
//        replyMarkup.setKeyboard(markup);
//
//        methods.sendMessage(chatId, text, TelegramAPI.TextMessageParseMode.MARKDOWN, false, 0, replyMarkup);
//    }
//
//
//}
