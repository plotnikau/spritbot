package com.rp.bot.service;

import com.rp.bot.model.Brand;
import com.rp.bot.model.FuelType;
import com.rp.bot.model.response.fuel.GasStation;
import com.rp.bot.repositories.UsersRepository;
import de.bots.TelegramAPI;
import de.bots.model.Location;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class PriceTrackingService {
    @Autowired
    private TelegramAPI telegramAPI;

    @Autowired
    private FuelService fuelService;

    @Autowired
    private UsersRepository usersRepository;

    private static final Logger LOG = Logger.getLogger(PriceTrackingService.class);

    public GasStation getHomeStation() {
        return homeStation;
    }

    private GasStation homeStation = null;

    @Scheduled(fixedDelay = 15 * 60 * 1000)
    public void trackPrices() {

        // track RP home station
        GasStation[] homeGasStations = fuelService.getGasStations(48.418607, 10.133025, 1);
        if (homeGasStations.length > 0) {
            homeStation = homeGasStations[0];
        }

        Set<String> priceTrackingChatIds = usersRepository.getPriceTrackingChatIds();
        for (String chatIdStr : priceTrackingChatIds) {
            Integer chatId = Integer.valueOf(chatIdStr);
            LOG.info("Tracking prices for chatId " + chatId);
            Location location = usersRepository.getPriceTrackingLocation(chatId);
            FuelType preferredFuelType = usersRepository.getFuelType(chatId);
//            Brand preferredBrand = usersRepository.getBrand(chatId);

            if (location != null) {
                GasStation[] gasStations = fuelService.getGasStations(location.getLatitude(), location.getLongitude(), FuelService.SEARCH_RADIUS_KM);
                GasStation lastPriceCheckResult = usersRepository.getPriceTrackingStation(chatId);

                if (lastPriceCheckResult == null) {
                    continue;
                }

                String stationId = lastPriceCheckResult.getId();

                for (GasStation gasStation : gasStations) {
                    if (gasStation.getId().equalsIgnoreCase(stationId)) {
                        // TODO: clean code, remove hack
                        if (Double.compare(gasStation.getDiesel(), lastPriceCheckResult.getDiesel()) != 0) {
                            usersRepository.setPriceTrackingStation(chatId, gasStation);

                            String response = FormattingHelper.prepareResponse(new GasStation[]{gasStation}, preferredFuelType, Brand.ALL);
                            telegramAPI.sendMessage(chatId, response, TelegramAPI.TextMessageParseMode.MARKDOWN);
                        }
                        break;
                    }
                }

            }

        }

    }
}
