package com.rp.bot.service;

import com.rp.bot.commands.KeyboardHelper;
import com.rp.bot.model.Brand;
import com.rp.bot.model.FuelType;
import com.rp.bot.model.response.fuel.GasStation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TelegramFuelService {

    private static final Logger LOG = Logger.getLogger(TelegramFuelService.class);

    @Autowired
    FuelService fuelService;

    @Autowired
    private KeyboardHelper keyboardHelper;

    public void getStationsForLocation(int chatId, Float lat, Float lon, FuelType preferredFuelType, Brand preferredBrand) {
        String responseText;
        GasStation[] gasStations = fuelService.getGasStations(lat, lon, FuelService.SEARCH_RADIUS_KM);

        responseText = FormattingHelper.prepareResponse(gasStations, preferredFuelType, preferredBrand);
        keyboardHelper.sendMessageWithLocationsKeyboard(chatId, responseText);
    }


}
