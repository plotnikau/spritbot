package com.rp.bot.service;

import com.rp.bot.model.FuelType;
import com.rp.bot.model.response.fuel.GasStation;
import com.rp.bot.model.response.fuel.StationFuelPrice;
import de.bots.model.Location;

public class UserPresets {
    Location lastLocation;
    Location homeLocation;
    GasStation stationForPriceTracking;
    StationFuelPrice pricesForTrackedStation;
    FuelType fuelType;

    public Location getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(Location lastLocation) {
        this.lastLocation = lastLocation;
    }

    public Location getHomeLocation() {
        return homeLocation;
    }

    public void setHomeLocation(Location homeLocation) {
        this.homeLocation = homeLocation;
    }

    public GasStation getStationForPriceTracking() {
        return stationForPriceTracking;
    }

    public void setStationForPriceTracking(GasStation stationForPriceTracking) {
        this.stationForPriceTracking = stationForPriceTracking;
    }

    public StationFuelPrice getPricesForTrackedStation() {
        return pricesForTrackedStation;
    }

    public void setPricesForTrackedStation(StationFuelPrice pricesForTrackedStation) {
        this.pricesForTrackedStation = pricesForTrackedStation;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    @Override
    public String toString() {
        return "UserPresets{" +
                "lastLocation=" + lastLocation +
                ", homeLocation=" + homeLocation +
                ", stationForPriceTracking=" + stationForPriceTracking +
                ", pricesForTrackedStation=" + pricesForTrackedStation +
                ", fuelType='" + fuelType + '\'' +
                '}';
    }
}
