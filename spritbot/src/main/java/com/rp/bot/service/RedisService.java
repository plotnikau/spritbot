package com.rp.bot.service;


import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.rp.bot.SpritBotProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RedisService {

    private RedisClient redisClient;
    private StatefulRedisConnection<String, String> redisConnection;

    @Autowired
    private void setProperties(SpritBotProperties botProperties) {
        redisClient = RedisClient.create(botProperties.getRedisUri());
    }

    public RedisCommands<String, String> connection() {
        if (redisConnection == null) {
            redisConnection = redisClient.connect();
        }
        return redisConnection.sync();
    }
}
