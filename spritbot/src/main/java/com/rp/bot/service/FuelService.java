package com.rp.bot.service;

import com.rp.bot.SpritBotProperties;
import com.rp.bot.model.response.fuel.FuelPriceApiResponse;
import com.rp.bot.model.response.fuel.GasStation;
import com.rp.bot.model.response.fuel.GasStationApiResponse;
import com.rp.bot.model.response.fuel.StationFuelPrice;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

@Component
public class FuelService {

    public static final int SEARCH_RADIUS_KM = 3;

    private final String fuelApiKey;
    private final String apiBaseUrl = "https://creativecommons.tankerkoenig.de/json/";

    private static final Logger LOG = Logger.getLogger(FuelService.class);
    private RestTemplate restTemplate;

    @Autowired
    public FuelService(SpritBotProperties botProperties) {
        this.fuelApiKey = botProperties.getFuelApiKey();
    }


    protected RestTemplate getRestTemplate() {
        if (null == restTemplate) {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

            SSLContext sslContext = null;
            try {
                sslContext = org.apache.http.ssl.SSLContexts.custom()
                        .loadTrustMaterial(null, acceptingTrustStrategy)
                        .build();
            } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
                e.printStackTrace();
            }

            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSSLSocketFactory(csf)
                    .build();

            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();

            requestFactory.setHttpClient(httpClient);

            restTemplate = new RestTemplate(requestFactory);
        }
        return restTemplate;
    }


    public GasStation[] getGasStations(double lat, double lng, int radius) {
        GasStationApiResponse response = getRestTemplate().getForObject(getGasStationsUrl()
                        + "?lat=" + lat
                        + "&lng=" + lng
                        + "&rad=" + radius
                        + "&sort=dist"
                        + "&type=all"
                        + "&apikey=" + fuelApiKey,
                GasStationApiResponse.class);

        if (response.getStations() != null) {
            LOG.info("received " + response.getStations().length + " gas stations");
        }

        return response.getStations();
    }

    private URI getGasStationsUrl() {
        return URI.create(apiBaseUrl + "list.php");
    }

    private URI getPricesUrl() {
        return URI.create(apiBaseUrl + "prices.php");
    }

    public StationFuelPrice getPricesForStation(String stationId) {
        //https://creativecommons.tankerkoenig.de/json/prices.php?ids=%5B"176bbaeb-43a8-40e5-ba5c-e061ca24b1e0"%5D&apikey=523aa195-d1cd-b5bd-210c-7791f758121b
//        String[] ids = {stationId, stationId};
//        Gson gson = new Gson();
//        String idsString = gson.toJson(ids);
//
//        Map<String, Object> data = new HashMap<>();
//        data.put("ids", ids);
//        data.put("apikey", fuelApiKey);
//
//        URI targetUrl = UriComponentsBuilder.fromUri(getPricesUrl())
//                .queryParam("ids", ids)
//                .queryParam("apikey", fuelApiKey)
//                .build()
//                .toUri();


        FuelPriceApiResponse response;
        String url = "https://creativecommons.tankerkoenig.de/json/prices.php?ids=%5B\"176bbaeb-43a8-40e5-ba5c-e061ca24b1e0\"%5D&apikey=523aa195-d1cd-b5bd-210c-7791f758121b";
        response = getRestTemplate().getForObject(url,
                FuelPriceApiResponse.class);

        LOG.debug("received" + response.getPrices().length + " stations prices");
        if (response.getPrices().length > 0) {
            return response.getPrices()[0];
        }
        else return null;
    }

}
