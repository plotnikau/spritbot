package com.rp.bot.monitoring;

public enum HealthStatus {
    UNKNOWN,
    OK,
    WARNING,
    ERROR,
}