package com.rp.bot.monitoring;

public class StatusDto {

    private HealthStatus status;
    private String message;

    public StatusDto(HealthStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public HealthStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "StatusDto{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}