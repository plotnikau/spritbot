package com.rp.bot.monitoring;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MonitoringController {

    private String versionNumber;

    @RequestMapping(value = "/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public InfoDto info() {
        versionNumber = getClass().getPackage().getImplementationVersion();
        return new InfoDto("tbot", "Telegram bot with spring boot", versionNumber);
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public StatusDto status() {
        return new StatusDto(HealthStatus.OK, "running");
    }

}