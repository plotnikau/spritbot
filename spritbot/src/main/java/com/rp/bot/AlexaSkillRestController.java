package com.rp.bot;

import com.rp.bot.service.FuelService;
import com.rp.bot.service.PriceTrackingService;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.UUID;

@RestController
@RequestMapping(path = "/setup")
public class AlexaSkillRestController {

    @Autowired
    PriceTrackingService priceTrackingService;

    private RestTemplate restTemplate;

    private static final Logger LOG = Logger.getLogger(FuelService.class);

    //https://www.carfu.com/login
    // ?state=abc
    // &client_id=alexa-skill
    // &scope=order_car%20basic_profile
    // &response_type=code
    // &redirect_uri=https%3A%2F%2Fpitangui.amazon.com%2Fspa%2Fskill%2Faccount-linking-status.html%3FvendorId%3DAAAAAAAAAAAAAA
    public String dosetup(@RequestParam("state") String state,
                        @RequestParam("client_id") String client_id,
                        @RequestParam("scope") String scope,
                        @RequestParam("response_type") String response_type,
                        @RequestParam("redirect_uri") String redirect_uri) {

        UUID token = UUID.randomUUID();


        LOG.info("generated token " + token);

        String response = getRestTemplate().getForObject(redirect_uri
                        + "?state=" + state
                        + "&access_token=" + token
                        + "&token_type=Bearer",
                String.class);

        LOG.info("called redirect url " + redirect_uri + "and got response " + response);
        return response;
    }

    @RequestMapping(method = RequestMethod.PUT)
    public String setup(@RequestBody TankstelleConfig config) {

        UUID token = UUID.randomUUID();

        LOG.info("New config: " + config.toString());


        LOG.info("generated token " + token);

        String response = getRestTemplate().getForObject(config.getRedirect_uri()
                        + "?state=" + config.getState()
                        + "&access_token=" + token
                        + "&token_type=Bearer",
                String.class);

        LOG.info("called redirect url " + config.getRedirect_uri() + " and got response " + response);
        return response;
    }

    protected RestTemplate getRestTemplate() {
        if (null == restTemplate) {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

            SSLContext sslContext = null;
            try {
                sslContext = org.apache.http.ssl.SSLContexts.custom()
                        .loadTrustMaterial(null, acceptingTrustStrategy)
                        .build();
            } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
                e.printStackTrace();
            }

            SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSSLSocketFactory(csf)
                    .build();

            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();

            requestFactory.setHttpClient(httpClient);

            restTemplate = new RestTemplate(requestFactory);
        }
        return restTemplate;
    }
}
