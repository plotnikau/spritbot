package com.rp.bot;

import de.bots.BotProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("bot")
public class SpritBotProperties extends BotProperties{

	private String fuelApiKey = "fuelApiKey";
	private String redisPassword = "secretpassword";

	public String getFuelApiKey() {
		return fuelApiKey;
	}

	public void setFuelApiKey(String fuelApiKey) {
		this.fuelApiKey = fuelApiKey;
	}

	public String getRedisPassword() {
		return redisPassword;
	}

	public void setRedisPassword(String redisPassword) {
		this.redisPassword = redisPassword;
	}
}