package com.rp.bot;

import com.rp.bot.model.response.fuel.GasStation;
import com.rp.bot.service.PriceTrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PriceRestController {

    @Autowired
    PriceTrackingService priceTrackingService;

    @RequestMapping(value = "/e5", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String e5() {
        GasStation homeStation = priceTrackingService.getHomeStation();
        if (homeStation != null) {
            return homeStation.getE5().toString();
        }
        return "0.0";
    }

    @RequestMapping(value = "/e10", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String e10() {
        GasStation homeStation = priceTrackingService.getHomeStation();
        if (homeStation != null) {
            return homeStation.getE10().toString();
        }
        return "0.0";
    }

    @RequestMapping(value = "/diesel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String diesel() {
        GasStation homeStation = priceTrackingService.getHomeStation();
        if (homeStation != null) {
            return homeStation.getDiesel().toString();
        }
        return "0.0";
    }


}
