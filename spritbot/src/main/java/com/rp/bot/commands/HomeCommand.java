package com.rp.bot.commands;

import com.rp.bot.model.Brand;
import com.rp.bot.model.FuelType;
import com.rp.bot.repositories.UsersRepository;
import com.rp.bot.service.TelegramFuelService;
import de.bots.Command;
import de.bots.model.Location;
import de.bots.model.Update;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HomeCommand implements Command{

    private static final Logger LOG = Logger.getLogger(HomeCommand.class);

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    TelegramFuelService telegramFuelService;

    @Autowired
    KeyboardHelper keyboardHelper;

    @Override
    public boolean isActive(int chatId) {

        return false;
    }

    @Override
    public boolean canHandle(int chatId, Update update) {

        String text = update.getMessage().getText();

        if (text != null && text.toLowerCase().startsWith("home")) {
            return true;
        }

        return false;
    }

    @Override
    public void handle(Update update) {

        int chatId = update.getMessage().getChat().getId();
        String text = update.getMessage().getText();

        if (SpritBotCommand.HOME.toString().equalsIgnoreCase(text)) {

            Location location = usersRepository.getHomeLocation(chatId);
            FuelType preferredFuelType = usersRepository.getFuelType(chatId);
            Brand preferredBrand = usersRepository.getBrand(chatId);
            if (location != null) {
                telegramFuelService.getStationsForLocation(chatId, location.getLatitude(), location.getLongitude(), preferredFuelType, preferredBrand);
                return;
            }

        }

        keyboardHelper.sendMessageWithLocationsKeyboard(chatId, "First click \"Settings\" and set your home location");

    }


}
