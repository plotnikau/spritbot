package com.rp.bot.commands;

import com.rp.bot.repositories.UsersRepository;
import de.bots.Command;
import de.bots.model.Location;
import de.bots.model.Update;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class SetHomeCommand implements Command {

    private static final Logger LOG = Logger.getLogger(SetHomeCommand.class);

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private KeyboardHelper keyboardHelper;

    private HashMap <Integer, Boolean> activeMap = new HashMap<>();

    @Override
    public boolean isActive(int chatId) {

        Boolean active = activeMap.get(chatId);
        return Boolean.TRUE.equals(active);
    }

    @Override
    public boolean canHandle(int chatId, Update update) {

        String text = update.getMessage().getText();

        if (SpritBotCommand.SET_HOME.toString().equalsIgnoreCase(text)) {
            return true;
        }

        return false;
    }

    @Override
    public void handle(Update update) {

        int chatId = update.getMessage().getChat().getId();
        String text = update.getMessage().getText();

        if (SpritBotCommand.SET_HOME.toString().equalsIgnoreCase(text)) {

            activeMap.put(chatId, true);

            keyboardHelper.sendMessageWithLocationRequestKeyboard(chatId, "Which location shall be set as home? (send a location to me)");
        }
        else {
            Location location = update.getMessage().getLocation();
            if (location == null) {
                keyboardHelper.sendMessageWithSettingsKeyboard(chatId, "Not a location!");
            }
            else {
                usersRepository.setHomeLocation(chatId, location);
                keyboardHelper.sendMessageWithSettingsKeyboard(chatId, "Home location set to " + location.getLatitude() + ", " + location.getLongitude());
            }
            activeMap.put(chatId, false);
        }
    }


}
