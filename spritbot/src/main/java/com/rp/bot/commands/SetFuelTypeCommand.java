package com.rp.bot.commands;

import com.rp.bot.model.FuelType;
import com.rp.bot.repositories.UsersRepository;
import de.bots.Command;
import de.bots.model.Update;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SetFuelTypeCommand implements Command{

    private static final Logger LOG = Logger.getLogger(SetFuelTypeCommand.class);

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    KeyboardHelper keyboardHelper;

    private boolean active = false;

    @Override
    public boolean isActive(int chatId) {

        return active;
    }

    @Override
    public boolean canHandle(int chatId, Update update) {

        String text = update.getMessage().getText();

        if (SpritBotCommand.SET_FUEL_TYPE.toString().equalsIgnoreCase(text)) {
            return true;
        }

        return false;
    }

    @Override
    public void handle(Update update) {

        int chatId = update.getMessage().getChat().getId();
        String text = update.getMessage().getText();

        if (SpritBotCommand.SET_FUEL_TYPE.toString().equalsIgnoreCase(text)) {
            active = true;

            keyboardHelper.sendFuelTypeKeyboard(chatId);
            return;
        }

        else {
            parseAndSetFuelType(chatId, text);

            active = false;
            return;
        }

    }

    private void parseAndSetFuelType(int chatId, String text) {

        FuelType fuelType = FuelType.valueOf(text.toUpperCase());
        if (fuelType == null) {
            fuelType = FuelType.ALL;
        }
        usersRepository.setFuelType(chatId, fuelType);

        keyboardHelper.sendMessageWithSettingsKeyboard(chatId, "Fuel type set to " + fuelType.toString());
    }


}
