package com.rp.bot.commands;

public enum SpritBotCommand {
    HOME("Home"),
    SET_HOME("Set home location"),
    ENABLE_PRICE_TRACKING("Enable price tracking"),
    DISABLE_PRICE_TRACKING("Disable price tracking"),
    SETTINGS("Settings"),
    START("/start"),
    BACK("Back"),
    SET_FUEL_TYPE("Set fuel type"),
    SET_BRAND("Set brand");

    private final String command;

    SpritBotCommand(final String text) {
        this.command = text;
    }

    @Override
    public String toString() {
        return command;
    }
}
