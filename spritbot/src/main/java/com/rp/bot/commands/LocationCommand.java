package com.rp.bot.commands;

import com.rp.bot.model.Brand;
import com.rp.bot.model.FuelType;
import com.rp.bot.repositories.UsersRepository;
import com.rp.bot.service.TelegramFuelService;
import de.bots.Command;
import de.bots.model.Location;
import de.bots.model.Update;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LocationCommand implements Command{

    private static final Logger LOG = Logger.getLogger(LocationCommand.class);

    @Autowired
    TelegramFuelService telegramFuelService;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public boolean isActive(int chatId) {

        return false;
    }

    @Override
    public boolean canHandle(int chatId, Update update) {

        if (update.getMessage().getLocation() != null) {
            return true;
        }

        return false;
    }

    @Override
    public void handle(Update update) {

        int chatId = update.getMessage().getChat().getId();
        Location location = update.getMessage().getLocation();

        usersRepository.setLastLocation(chatId, location);

        FuelType preferredFuelType = usersRepository.getFuelType(chatId);
        Brand preferredBrand = usersRepository.getBrand(chatId);

        telegramFuelService.getStationsForLocation(chatId, location.getLatitude(), location.getLongitude(), preferredFuelType, preferredBrand);

    }



}
