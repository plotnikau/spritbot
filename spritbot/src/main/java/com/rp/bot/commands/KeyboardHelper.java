package com.rp.bot.commands;

import com.rp.bot.model.Brand;
import com.rp.bot.model.FuelType;
import com.rp.bot.model.response.fuel.GasStation;
import de.bots.TelegramAPI;
import de.bots.model.KeyboardButton;
import de.bots.model.ReplyKeyboardMarkup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class KeyboardHelper {

    @Autowired
    private TelegramAPI telegramAPI;

    public void sendMessageWithLocationsKeyboard(int chatId, String text) {
        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
        replyMarkup.setOne_time_keyboard(false);
        replyMarkup.setResize_keyboard(true);

        KeyboardButton requestLocationButton = new KeyboardButton("Near me");
        requestLocationButton.setRequest_location(true);

        KeyboardButton[][] markup = {
                {new KeyboardButton(SpritBotCommand.HOME.toString())},
                {requestLocationButton},
                {new KeyboardButton(SpritBotCommand.SETTINGS.toString())}
        };

        replyMarkup.setKeyboard(markup);

        telegramAPI.sendMessage(chatId, text, TelegramAPI.TextMessageParseMode.MARKDOWN, false, 0, replyMarkup);
    }

    public void sendMessageWithLocationRequestKeyboard(int chatId, String text) {
        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
        replyMarkup.setOne_time_keyboard(false);
        replyMarkup.setResize_keyboard(true);

        KeyboardButton requestLocationButton = new KeyboardButton("Near me");
        requestLocationButton.setRequest_location(true);

        KeyboardButton[][] markup = {
                {requestLocationButton},
                {new KeyboardButton(SpritBotCommand.SETTINGS.toString())}
        };

        replyMarkup.setKeyboard(markup);

        telegramAPI.sendMessage(chatId, text, TelegramAPI.TextMessageParseMode.MARKDOWN, false, 0, replyMarkup);
    }

    public void sendFuelTypeKeyboard(int chatId) {
        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
        replyMarkup.setOne_time_keyboard(false);
        replyMarkup.setResize_keyboard(true);

        KeyboardButton[][] markup = {
                {new KeyboardButton(FuelType.DIESEL.toString())},
                {new KeyboardButton(FuelType.E5.toString())},
                {new KeyboardButton(FuelType.E10.toString())},
                {new KeyboardButton(FuelType.ALL.toString())}
        };

        replyMarkup.setKeyboard(markup);

        telegramAPI.sendMessage(chatId, "What is your preferred fuel type?", false, 0, replyMarkup);
    }

    public void sendBrandsKeyboard(int chatId) {
        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
        replyMarkup.setOne_time_keyboard(false);
        replyMarkup.setResize_keyboard(true);

        KeyboardButton[][] markup = {
                {new KeyboardButton(Brand.SHELL.toString()),
                new KeyboardButton(Brand.ARAL.toString())},
                {new KeyboardButton(Brand.ESSO.toString()),
                new KeyboardButton(Brand.TOTAL.toString())},
                {new KeyboardButton(Brand.ALL.toString())}
        };

        replyMarkup.setKeyboard(markup);

        telegramAPI.sendMessage(chatId, "What is your preferred gas station brand?", false, 0, replyMarkup);
    }

    public void sendSettingsKeyboard(int chatId) {
        sendMessageWithSettingsKeyboard(chatId, null);
    }

    public void sendMessageWithSettingsKeyboard(int chatId, String message) {
        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
        replyMarkup.setOne_time_keyboard(false);
        replyMarkup.setResize_keyboard(true);

        KeyboardButton[][] markup = {
                {new KeyboardButton(SpritBotCommand.SET_HOME.toString())},
                {new KeyboardButton(SpritBotCommand.SET_FUEL_TYPE.toString()),
                new KeyboardButton(SpritBotCommand.SET_BRAND.toString())},
                {new KeyboardButton(SpritBotCommand.ENABLE_PRICE_TRACKING.toString()),
                new KeyboardButton(SpritBotCommand.DISABLE_PRICE_TRACKING.toString())},
                {new KeyboardButton(SpritBotCommand.BACK.toString())}
        };

        replyMarkup.setKeyboard(markup);

        if (message == null) {
            telegramAPI.sendMessage(chatId, "What shall we do next?", false, 0, replyMarkup);
        }
        else {
            telegramAPI.sendMessage(chatId, message, false, 0, replyMarkup);
        }
    }

    public void sendMessageWithStationsKeyboard(int chatId, String message, GasStation[] gasStations) {

        ReplyKeyboardMarkup replyMarkup = new ReplyKeyboardMarkup();
        replyMarkup.setOne_time_keyboard(false);
        replyMarkup.setResize_keyboard(true);

        KeyboardButton[][] markup = new KeyboardButton[gasStations.length][1];
        for (int i = 0; i < gasStations.length; i++) {
            markup[i][0] = new KeyboardButton(gasStations[i].getName());
        }

        replyMarkup.setKeyboard(markup);

        if (message == null) {
            telegramAPI.sendMessage(chatId, "Select station", false, 0, replyMarkup);
        }
        else {
            telegramAPI.sendMessage(chatId, message, false, 0, replyMarkup);
        }
    }

}
