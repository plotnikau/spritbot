package com.rp.bot.commands;

import com.rp.bot.model.response.fuel.GasStation;
import com.rp.bot.repositories.UsersRepository;
import com.rp.bot.service.FuelService;
import de.bots.Command;
import de.bots.TelegramAPI;
import de.bots.model.Location;
import de.bots.model.Update;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class EnablePriceTrackingCommand implements Command {

    private static final Logger LOG = Logger.getLogger(EnablePriceTrackingCommand.class);

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private KeyboardHelper keyboardHelper;

    @Autowired
    private FuelService fuelService;

    @Autowired
    private TelegramAPI telegramAPI;

    private HashMap <Integer, Boolean> activeMap = new HashMap<>();

    @Override
    public boolean isActive(int chatId) {

        Boolean active = activeMap.get(chatId);
        return Boolean.TRUE.equals(active);
    }

    @Override
    public boolean canHandle(int chatId, Update update) {

        String text = update.getMessage().getText();

        if (SpritBotCommand.ENABLE_PRICE_TRACKING.toString().equalsIgnoreCase(text)) {
            return true;
        }

        return false;
    }

    @Override
    public void handle(Update update) {

        int chatId = update.getMessage().getChat().getId();
        String text = update.getMessage().getText();

        if (SpritBotCommand.ENABLE_PRICE_TRACKING.toString().equalsIgnoreCase(text)) {

            activeMap.put(chatId, true);

            keyboardHelper.sendMessageWithLocationsKeyboard(chatId, "Send a location for price tracking");
        }
        else {
            Location location = update.getMessage().getLocation();
            if (location != null) {
                usersRepository.setPriceTrackingLocation(chatId, location);
                telegramAPI.sendMessage(chatId, "Location for price tracking set to " + location.getLatitude() + ", " + location.getLongitude());
                requestStationSelection(chatId, location);
            }
            else if (SpritBotCommand.HOME.toString().equalsIgnoreCase(text)) {
                Location homeLocation = usersRepository.getHomeLocation(chatId);
                usersRepository.setPriceTrackingLocation(chatId, homeLocation);
                telegramAPI.sendMessage(chatId, "Location for price tracking set to " + homeLocation.getLatitude() + ", " + homeLocation.getLongitude());
                requestStationSelection(chatId, homeLocation);
            }
            else {

                GasStation[] gasStationsAtTrackedLocation = usersRepository.getGasStationsAtTrackedLocation(chatId);
                for (GasStation gasStation : gasStationsAtTrackedLocation) {
                    if (gasStation.getName().equalsIgnoreCase(text)) {
                        usersRepository.setPriceTrackingStation(chatId, gasStation);
                        keyboardHelper.sendMessageWithSettingsKeyboard(chatId, "Station for price tracking set to " + gasStation.getMdFormattedDescription());

                        LOG.info("price tracking enabled for chatId " + chatId);

                        activeMap.put(chatId, false);
                        return;
                    }
                }

                keyboardHelper.sendMessageWithSettingsKeyboard(chatId, "Neither station nor location!");
                activeMap.put(chatId, false);
            }
        }
    }


    private void requestStationSelection(int chatId, Location location) {
        GasStation[] gasStations = fuelService.getGasStations(location.getLatitude(), location.getLongitude(), FuelService.SEARCH_RADIUS_KM);
        if (gasStations.length > 0) {
            usersRepository.setGasStationsAtTrackedLocation(chatId, gasStations);
            keyboardHelper.sendMessageWithStationsKeyboard(chatId, "Select station for price tracking", gasStations);
        }
        else {
            keyboardHelper.sendMessageWithSettingsKeyboard(chatId, "No gas stations here!");
            activeMap.put(chatId, false);
        }
    }


}
