package com.rp.bot.commands;

import com.rp.bot.model.Brand;
import com.rp.bot.model.FuelType;
import com.rp.bot.repositories.UsersRepository;
import de.bots.Command;
import de.bots.model.Update;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SetBrandCommand implements Command{

    private static final Logger LOG = Logger.getLogger(SetBrandCommand.class);

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    KeyboardHelper keyboardHelper;

    private boolean active = false;

    @Override
    public boolean isActive(int chatId) {

        return active;
    }

    @Override
    public boolean canHandle(int chatId, Update update) {

        String text = update.getMessage().getText();

        if (SpritBotCommand.SET_BRAND.toString().equalsIgnoreCase(text)) {
            return true;
        }

        return false;
    }

    @Override
    public void handle(Update update) {

        int chatId = update.getMessage().getChat().getId();
        String text = update.getMessage().getText();

        if (SpritBotCommand.SET_BRAND.toString().equalsIgnoreCase(text)) {
            active = true;

            keyboardHelper.sendBrandsKeyboard(chatId);
            return;
        }

        else {
            parseAndSetBrand(chatId, text);

            active = false;
            return;
        }

    }

    private void parseAndSetBrand(int chatId, String text) {

        Brand brand = Brand.ALL;

        if (text != null) {
            try {
                brand = Brand.valueOf(text.toUpperCase());
            }
            catch (IllegalArgumentException e) {
                LOG.error("brand parsing exception: ", e);
            }

            if (brand == null) {
                brand = Brand.ALL;
            }
        }
        usersRepository.setBrand(chatId, brand);

        keyboardHelper.sendMessageWithSettingsKeyboard(chatId, "Brand set to " + brand.toString());
    }


}
