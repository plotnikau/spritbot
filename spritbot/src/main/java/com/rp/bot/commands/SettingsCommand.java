package com.rp.bot.commands;

import de.bots.Command;
import de.bots.model.Update;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SettingsCommand implements Command {

    private static final Logger LOG = Logger.getLogger(SettingsCommand.class);

    @Autowired
    KeyboardHelper keyboardHelper;

    @Override
    public boolean isActive(int chatId) {

        return false;
    }

    @Override
    public boolean canHandle(int chatId, Update update) {

        String text = update.getMessage().getText();

        if (SpritBotCommand.SETTINGS.toString().equalsIgnoreCase(text)) {
            return true;
        }

        return false;
    }

    @Override
    public void handle(Update update) {

        int chatId = update.getMessage().getChat().getId();
        String text = update.getMessage().getText();

        if (SpritBotCommand.SETTINGS.toString().equalsIgnoreCase(text)) {

            keyboardHelper.sendSettingsKeyboard(chatId);
        }
    }


}
