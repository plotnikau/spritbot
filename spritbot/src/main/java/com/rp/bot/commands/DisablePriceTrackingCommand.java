package com.rp.bot.commands;

import com.rp.bot.model.response.fuel.GasStation;
import com.rp.bot.repositories.UsersRepository;
import com.rp.bot.service.FuelService;
import de.bots.Command;
import de.bots.TelegramAPI;
import de.bots.model.Location;
import de.bots.model.Update;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class DisablePriceTrackingCommand implements Command {

    private static final Logger LOG = Logger.getLogger(DisablePriceTrackingCommand.class);

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private KeyboardHelper keyboardHelper;

    @Override
    public boolean isActive(int chatId) {
        return false;
    }

    @Override
    public boolean canHandle(int chatId, Update update) {

        String text = update.getMessage().getText();

        if (SpritBotCommand.DISABLE_PRICE_TRACKING.toString().equalsIgnoreCase(text)) {
            return true;
        }

        return false;
    }

    @Override
    public void handle(Update update) {

        int chatId = update.getMessage().getChat().getId();

        usersRepository.removePriceTracking(chatId);

        LOG.info("price tracking disabled for chatId " + chatId);

        keyboardHelper.sendMessageWithSettingsKeyboard(chatId, "Price tracking disabled");
    }

}
