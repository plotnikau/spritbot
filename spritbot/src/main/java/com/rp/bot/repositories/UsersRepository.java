package com.rp.bot.repositories;

import com.google.gson.Gson;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.rp.bot.model.Brand;
import com.rp.bot.model.FuelType;
import com.rp.bot.model.PriceTrackingSettings;
import com.rp.bot.model.response.fuel.GasStation;
import com.rp.bot.service.RedisService;
import de.bots.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class UsersRepository {

    private static final String SPRITBOT_USERREPO_PREFIX = "spritbot:userpresets:";
    @Autowired
    RedisService redisService;
    private Gson gson = new Gson();

    public void setLastLocation(int chatId, Location location) {

        redis().set(lastLocationKey(chatId), gson.toJson(location));
    }

    public Location getLastLocation(int chatId) {

        String value = redis().get(lastLocationKey(chatId));

        Location location = null;
        if (value != null) {
            location = gson.fromJson(value, Location.class);
        }

        return location;
    }

    public void setFuelType(int chatId, FuelType fuelType) {

        redis().set(fuelTypeKey(chatId), fuelType.toString());
    }

    public FuelType getFuelType(int chatId) {

        String value = redis().get(fuelTypeKey(chatId));
        if (value != null) {
            return FuelType.valueOf(value.toUpperCase());
        } else {
            return null;
        }
    }

    public void setBrand(int chatId, Brand brand) {

        redis().set(brandKey(chatId), brand.toString());
    }

    public Brand getBrand(int chatId) {

        String value = redis().get(brandKey(chatId));
        if (value != null) {
            return Brand.valueOf(value.toUpperCase());
        } else {
            return Brand.ALL;
        }
    }


    public void setHomeLocation(int chatId, Location location) {

        redis().set(homeLocationKey(chatId), gson.toJson(location));
    }

    public Location getHomeLocation(int chatId) {

        String value = redis().get(homeLocationKey(chatId));

        Location location = null;
        if (value != null) {
            location = gson.fromJson(value, Location.class);
        }

        return location;
    }

    public Set<String> getPriceTrackingChatIds() {

        return redis().smembers(priceTrackingChatIdsKey());
    }

    public PriceTrackingSettings createOrGetPriceTrackingSettings(int chatId) {
        String json = redis().get(priceTrackingSettingsKey(chatId));
        PriceTrackingSettings priceTrackingSettings;
        if (json == null) {
            priceTrackingSettings = new PriceTrackingSettings();
            priceTrackingSettings.setChatId(chatId);

            redis().set(priceTrackingSettingsKey(chatId), gson.toJson(priceTrackingSettings));
            redis().sadd(priceTrackingChatIdsKey(), String.valueOf(chatId));
        }
        else {
            priceTrackingSettings = gson.fromJson(json, PriceTrackingSettings.class);
        }
        return priceTrackingSettings;
    }

    public void removePriceTracking(int chatId) {
        redis().del(priceTrackingSettingsKey(chatId));
        redis().srem(priceTrackingChatIdsKey(), String.valueOf(chatId));
    }

    public void setPriceTrackingLocation(int chatId, Location location) {
        PriceTrackingSettings settings = createOrGetPriceTrackingSettings(chatId);
        settings.setLocation(location);
        redis().set(priceTrackingSettingsKey(chatId), gson.toJson(settings));
    }

    public Location getPriceTrackingLocation(int chatId) {
        PriceTrackingSettings settings = createOrGetPriceTrackingSettings(chatId);
        return settings.getLocation();
    }

    public void setGasStationsAtTrackedLocation(int chatId, GasStation[] gasStations) {
        PriceTrackingSettings settings = createOrGetPriceTrackingSettings(chatId);
        settings.setGasStationsAtLocation(gasStations);
        redis().set(priceTrackingSettingsKey(chatId), gson.toJson(settings));
    }

    public GasStation[] getGasStationsAtTrackedLocation(int chatId) {
        PriceTrackingSettings settings = createOrGetPriceTrackingSettings(chatId);
        return settings.getGasStationsAtLocation();
    }

    public void setPriceTrackingStation(int chatId, GasStation gasStation) {
        PriceTrackingSettings settings = createOrGetPriceTrackingSettings(chatId);
        settings.setGasStation(gasStation);
        redis().set(priceTrackingSettingsKey(chatId), gson.toJson(settings));
    }

    public GasStation getPriceTrackingStation(int chatId) {
        PriceTrackingSettings settings = createOrGetPriceTrackingSettings(chatId);
        return settings.getGasStation();
    }

    private String lastLocationKey(int chatId) {
        return SPRITBOT_USERREPO_PREFIX + chatId + ":location:last";
    }

    private String homeLocationKey(int chatId) {
        return SPRITBOT_USERREPO_PREFIX + chatId + ":location:home";
    }

    private String priceTrackingChatIdsKey() {
        return SPRITBOT_USERREPO_PREFIX + ":priceTracking:chatIds";
    }

    private String priceTrackingSettingsKey(int chatId) {
        return SPRITBOT_USERREPO_PREFIX + chatId + ":priceTracking:settings";
    }

    private String fuelTypeKey(int chatId) {
        return SPRITBOT_USERREPO_PREFIX + chatId + ":fuelType";
    }

    private String brandKey(int chatId) {
        return SPRITBOT_USERREPO_PREFIX + chatId + ":brand";
    }

    private RedisCommands<String, String> redis() {
        return redisService.connection();
    }

}
