package com.rp.bot.model.response.fuel;

public class StationFuelPrice {
    /*
      "status": "open",
      "e5": 1.279,
      "e10": 1.259,
      "diesel": 1.059
     */
    private String status;
    private Double diesel;
    private Double e5;
    private Double e10;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getDiesel() {
        return diesel;
    }

    public void setDiesel(Double diesel) {
        this.diesel = diesel;
    }

    public Double getE5() {
        return e5;
    }

    public void setE5(Double e5) {
        this.e5 = e5;
    }

    public Double getE10() {
        return e10;
    }

    public void setE10(Double e10) {
        this.e10 = e10;
    }

    @Override
    public String toString() {
        return "StationFuelPrice{" +
                "status='" + status + '\'' +
                ", diesel=" + diesel +
                ", e5=" + e5 +
                ", e10=" + e10 +
                '}';
    }
}
