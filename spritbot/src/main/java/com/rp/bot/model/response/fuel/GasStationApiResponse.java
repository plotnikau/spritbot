package com.rp.bot.model.response.fuel;

import java.util.Arrays;

public class GasStationApiResponse {
    private String status;
    private boolean ok;
    private String license;
    private String data;
    private GasStation[] stations;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean getOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public GasStation[] getStations() {
        return stations;
    }

    public void setStations(GasStation[] stations) {
        this.stations = stations;
    }

    @Override
    public String toString() {
        return "GasStationApiResponse{" +
                "status='" + status + '\'' +
                ", ok='" + ok + '\'' +
                ", license='" + license + '\'' +
                ", data='" + data + '\'' +
                ", stations=" + Arrays.toString(stations) +
                '}';
    }
}
