package com.rp.bot.model.response.fuel;

import java.util.Arrays;

public class FuelPriceApiResponse {
    private boolean ok;
    private String license;
    private String data;
    private StationFuelPrice[] prices;

//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public StationFuelPrice[] getPrices() {
        return prices;
    }

    public void setPrices(StationFuelPrice[] prices) {
        this.prices = prices;
    }

    @Override
    public String toString() {
        return "FuelPriceApiResponse{" +
                //"status='" + status + '\'' +
                "ok=" + ok +
                ", license='" + license + '\'' +
                ", data='" + data + '\'' +
                ", prices=" + Arrays.toString(prices) +
                '}';
    }
}
