package com.rp.bot.model;

import com.rp.bot.model.response.fuel.GasStation;
import de.bots.model.Location;

public class PriceTrackingSettings {
    private int chatId;
    private Location location;
    private GasStation[] gasStationsAtLocation;
    private GasStation gasStation;

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public GasStation getGasStation() {
        return gasStation;
    }

    public void setGasStation(GasStation gasStation) {
        this.gasStation = gasStation;
    }

    public GasStation[] getGasStationsAtLocation() {
        return gasStationsAtLocation;
    }

    public void setGasStationsAtLocation(GasStation[] gasStationsAtLocation) {
        this.gasStationsAtLocation = gasStationsAtLocation;
    }
}
