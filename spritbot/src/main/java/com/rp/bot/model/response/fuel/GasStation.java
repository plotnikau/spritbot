package com.rp.bot.model.response.fuel;

import org.apache.commons.lang3.text.WordUtils;

public class GasStation {
    /*
        "name": "TOTAL Nersingen",
        "lat": 48.416824,
        "lng": 10.102636,
        "brand": "TOTAL",
        "dist": 1.6,
        "price": 1.069,
        "diesel": 1.069,
        "e5": 1.069,
        "e10": 1.069,
        "id": "176bbaeb-43a8-40e5-ba5c-e061ca24b1e0",
        "street": "AN DER LEIBI",
        "houseNumber": "1",
        "postCode": 89278,
        "place": "NERSINGEN",
        "isOpen": true
     */
    private String name;
    private double lat;
    private double lng;
    private String brand;
    private double dist;
    private Double price;
    private Double diesel;
    private Double e5;
    private Double e10;
    private String id;
    private String street;
    private String houseNumber;
    private int postCode;
    private String place;
    private boolean isOpen;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiesel() {
        return diesel;
    }

    public void setDiesel(Double diesel) {
        this.diesel = diesel;
    }

    public Double getE5() {
        return e5;
    }

    public void setE5(Double e5) {
        this.e5 = e5;
    }

    public Double getE10() {
        return e10;
    }

    public void setE10(Double e10) {
        this.e10 = e10;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public String getMdFormattedDescription() {
        StringBuilder sb = new StringBuilder();
        if (this.getBrand() != null) {
            sb.append("*").append(this.getBrand()).append("*").append(" - ");
        }
        else {
            sb.append("*no brand*").append(" - ");
        }
        sb.append(this.getStreet()).append(" ").append(this.getHouseNumber()).append(", ").append(this.getPlace());

        return WordUtils.capitalizeFully(sb.toString());
    }

    @Override
    public String toString() {
        return "GasStation{" +
                "name='" + name + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", brand='" + brand + '\'' +
                ", dist=" + dist +
                ", price=" + price +
                ", diesel=" + diesel +
                ", e5=" + e5 +
                ", e10=" + e10 +
                ", id='" + id + '\'' +
                ", street='" + street + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", postCode=" + postCode +
                ", place='" + place + '\'' +
                ", isOpen=" + isOpen +
                '}';
    }
}
