package com.rp.bot.model;

public enum FuelType {
    E5("e5"), E10("e10"), DIESEL("diesel"), ALL("all");

    private final String text;
    private FuelType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }


}
