package com.rp.bot.model;

public enum Brand {
    SHELL("Shell"), ESSO("Esso"), ARAL("Aral"), TOTAL("Total"), ALL("All");

    private final String text;
    private Brand(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }


}
